import os

from orator import DatabaseManager, Model, SoftDeletes

DB_CONFIG = {
    'mysql': {
        'driver': 'mysql',
        'host': os.environ.get('DB_HOST'),
        'port': int(os.environ.get('DB_PORT')),
        'database': os.environ.get('DB_DATABASE'),
        'user': os.environ.get('DB_USER'),
        'password': os.environ.get('DB_PASSWORD'),
        'prefix': ''
    }
}

db = DatabaseManager(DB_CONFIG)
Model.set_connection_resolver(db)


class Storage(SoftDeletes, Model):
    __table__ = 'storage'
