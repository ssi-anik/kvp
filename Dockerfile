FROM python:3.7-slim
RUN apt-get update && apt-get install -y build-essential
RUN apt-get -y install nano curl default-libmysqlclient-dev
RUN mkdir -p /app
COPY requirements.txt /app
WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app
EXPOSE 80
CMD ["gunicorn", "-b", "0.0.0.0:80", "--workers=1", "--reload", "--access-logfile", "-", "--error-logfile", "-", "app:app"]