from flask import make_response, jsonify, Flask, request

from methods import now, add_to_now
from models import Storage

app = Flask(__name__)


# index page
@app.route('/', methods=['GET'], )
def index():
    return make_response(jsonify({
        'error': False,
        'message': 'KVP up & running',
    }), 200)


@app.route('/values', methods=['GET'])
def get_values():
    lookup = request.args.get('keys')

    q = Storage.where('ttl_till', '>', now()).order_by('id', 'desc')
    if lookup:
        lookup = lookup.split(',')
        q = q.where_in('kv_key', lookup)

    keys = q.lists('kv_value', 'kv_key')

    return make_response(jsonify(keys), 200)


@app.route('/values', methods=['POST'])
def add_value():
    data = request.get_json() or request.form

    key = data.get('key')
    key = key.strip() if key else ''

    value = data.get('value')
    value = value.strip() if value else ''

    if not key or not value:
        return make_response(jsonify({
            'error': True,
            'message': 'Either Key or Value is missing',
        }), 422)

    storage = Storage.where('kv_key', key).first()
    message = 'Key updated successfully'
    code = 202
    if not storage:
        message = 'Key added successfully'
        code = 201
        storage = Storage()

    storage.kv_key = key
    storage.kv_value = value
    storage.ttl_till = add_to_now(5)
    storage.save()

    return make_response(jsonify({
        'error': False,
        'message': message
    }), code)


@app.route('/values', methods=['PATCH'])
def update_values():
    data = request.get_json() or request.form
    s = {}
    for k, v in data.items():
        s[k] = v

    if not s:
        return make_response(jsonify({
            'error': True,
            'message': 'No key provided to update value.'
        }), 422)

    rows = Storage.where_in('kv_key', list(s.keys())).where('ttl_till', '>', now()).get()
    in_storage = rows.pluck('kv_key')
    missing = list(set(data.keys()) - set(list(in_storage)))

    r = {
        'updated': list(in_storage),
        'missing': missing
    }

    for row in rows:
        print(row, row.kv_key)
        row.kv_value = s[row.kv_key]
        row.ttl_till = add_to_now(5)
        row.save()

    return make_response(jsonify({
        'error': False,
        'message': 'Updated key-value pair',
        'data': r
    }), 200)
