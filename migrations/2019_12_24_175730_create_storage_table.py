from orator.migrations import Migration


class CreateStorageTable(Migration):

    def up(self):
        with self.schema.create('storage') as table:
            table.increments('id')
            table.string('kv_key')
            table.string('kv_value')
            table.datetime('ttl_till').nullable()
            table.soft_deletes()
            table.timestamps()

    def down(self):
        self.schema.drop('storage')
