import datetime


def now():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def add_to_now(minutes=None):
    if not minutes:
        minutes = 5

    return (datetime.datetime.now() + datetime.timedelta(minutes=minutes)).strftime("%Y-%m-%d %H:%M:%S")
