## Key-Value Pair in python

### Installation
- Clone this repo
- Copy `docker-compose.yml.example` to `docker-compose.yml` & Update required values
- Copy `orator.yml.example` to `orator.yml` & update required values.
- `docker-compose up -d --build`
- `docker-compose exec app bash`
- `orator migrate`

### QUESTION 
[Question link here](https://hackmd.io/xh1aTGItQCitvWUVJMwaYg)

### Solutions

- Get all available KV from storage
```bash
curl -X GET 'http://127.0.0.1:8000/values'
```

- Filter available KV from storage
```bash
curl -X GET 'http://127.0.0.1:8000/values?keys=key1'
```

- Add new KV to storage
```bash
curl -X POST 'http://127.0.0.1:8000/values' -H 'Content-Type: application/json' -d '{"key": "key13","value": "value13"}'
```

- Update set of KV to storage
```bash
curl -X PATCH 'http://127.0.0.1:8000/values' -H 'Content-Type: application/json' -d '{"key1": "new-value1","key2": "new-value-2"}'
```